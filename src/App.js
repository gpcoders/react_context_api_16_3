import React, { Component } from 'react';

import Person from './Person'

class App extends Component {
    render() {
        return (
            <div>
                <p>I'm the app</p>
                <Person />
            </div>
        );
    }
}
export default App;