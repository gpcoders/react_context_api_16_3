import React from 'react'

import { Consumer } from './index'

const gpCoders = () => {
    return(
        <React.Fragment>
            <Consumer>
                {(gpcoders) => {
                    return (
                        <p>{gpcoders.state.gpcoders ? gpcoders.state.gpcoders : "hello Gpcoders"}</p>
                    )
                }}

            </Consumer>
        </React.Fragment>
    )
}

export default gpCoders