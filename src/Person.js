import React, { Component } from 'react'

import { Consumer } from './index'

class Person extends Component {
    render() {
        return (
            <div className="person">
                <Consumer>
                    {(context) => {
                        return (
                        <React.Fragment>
                            <p>Age: {context.state.age}</p>
                            <p>Name: {context.state.name}</p>
                            <button onClick={context.growAYearOlder}>🍰🍥🎂</button>
                        </React.Fragment>
                    )}}
                </Consumer>
            </div>
        )
    }
}

export default Person;