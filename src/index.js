import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route } from 'react-router-dom'

import App from './App';
import GpCoders from './GpCoders'

// first we will make a new context
export const { Provider, Consumer } = React.createContext();

// Then create a provider Component
class GpProvider extends Component {
    state = {
        name: 'Wes',
        age: 100,
        cool: true,
        gpcoders: "I am gpcoders"
    }

    growAYearOlder = () => {
        this.setState({
            age: this.state.age + 1
        })
    }

    render() {
        return (
            <Provider value={{
                state: this.state,
                growAYearOlder: this.growAYearOlder
            }}>
                {this.props.children}
            </Provider>
        )
    }
}

const Routes = () => (
    <BrowserRouter>
        <React.Fragment>
            <Route path="/" exact component={App} />
            <Route path="/gpcoders" component={GpCoders} />
        </React.Fragment>
    </BrowserRouter>
)

ReactDOM.render(<GpProvider><Routes /></GpProvider>, document.getElementById('root'));
registerServiceWorker();
